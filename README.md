# deepcypher-helm chart

All of DeepCypher's resources can be easily, and reproducibly deployed as versioned helm-charts. This repository contains all such files, and automatically builds them and packages them into the gitlab helm package registry.
The helm-package registry for this repository is available for browsing at: https://gitlab.com/deepcypher/dc-helm/-/packages/

## Requirements

- [Helm](https://helm.sh/)
- [Kubernetes](https://kubernetes.io/) cluster (or [minikube](https://minikube.sigs.k8s.io/docs/))

## Usage

To use this helm chart add the repo with the package:
```bash
helm repo add dc-helm https://gitlab.com/api/v4/projects/26494192/packages/helm/stable
```

To ensure helms local index has indexed all versions
```bash
helm repo update dc-helm
```

To see all available versions
```bash
helm search repo dc-helm/deepcypher --versions
```

To deploy the package (the helm chart will be in default namespace but the resources will be deployed in their own namespace):
```bash
helm install dc dc-helm/deepcypher --set namespace.create=true
```

To list installed helm packages and their versions
```bash
helm list -A
```

To upgrade the helm package version to latest
```bash
helm upgrade dc dc-helm/deepcypher
```

To remove the package:
```bash
helm uninstall dc
```

To remove the repo:
```bash
helm repo remove dc-helm
```

### Makefile

For local development you may use the makefile to automate a lot of the steps for local testing. The makefile will modify your local kube config to point to a locally spun up minikube instance by default. However if you do not call the minikube target (called by default if target not specified) it may also be used for production purposes.

## Troubleshooting

- "Error: INSTALLATION FAILED: cannot re-use a name that is still in use", the name of the helm chart which most likeley is "dc" already exists. Remove it to be able to install helm again, or consider [helm upgrade](https://helm.sh/docs/helm/helm_upgrade/):
```bash
helm uninstall dc && sleep 60 && helm install dc dc-helm/deepcypher
```
